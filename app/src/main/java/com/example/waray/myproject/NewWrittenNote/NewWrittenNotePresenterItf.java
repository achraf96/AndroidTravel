package com.example.waray.myproject.NewWrittenNote;

import com.example.waray.myproject.metier.WrittenNote;

/**
 * Created by Achrafpc on 16/12/2017.
 */

public interface NewWrittenNotePresenterItf {

    public WrittenNote createWrittenNote(String date, double lat, double lng, String title, String description, long idVoyage);

}
