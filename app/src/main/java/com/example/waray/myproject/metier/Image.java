package com.example.waray.myproject.metier;

import java.io.Serializable;

/**
 * Created by Waray on 04/11/2017.
 */

public class Image implements Serializable {
    long id;
    String nom;
    String url;
    double lat;
    double lng;
    long idVoyage;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(long idVoyage) {
        this.idVoyage = idVoyage;
    }

    public Image(Image image) {
        this.id = image.getId();
        this.nom = image.getNom();
        this.url = image.getUrl();
        this.lat = image.getLat();
        this.lng = image.getLng();
        this.idVoyage = image.getIdVoyage();
    }

    public Image() {
    }
}
