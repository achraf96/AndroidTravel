package com.example.waray.myproject.metier;

/**
 * Created by Waray on 11/10/2017.
 */

public class Voyage {

    long id;
    String titre;
    String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Voyage(long id, String titre, String description) {
        this.id = id;
        this.titre = titre;
        this.description = description;
    }

    public Voyage(Voyage v) {
        this.id = v.getId();
        this.titre = v.getTitre();
        this.description = v.getDescription();
    }

    public Voyage() {
    }

    @Override
    public String toString() {
        return titre;
    }

}
