package com.example.waray.myproject.NewTravel;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.TravelManagerItf;
import com.example.waray.myproject.metier.Voyage;

/**
 * Created by Waray on 18/11/2017.
 */

public class NewTravelPresenter implements NewTravelPresenterItf {

    protected NewTravelActivityItf view;

    protected TravelManagerItf travelManager;

    public NewTravelPresenter(NewTravelActivity view){

        this.view = view;

        travelManager = AppManager.getInstance().getTravelManager();
    }

    @Override
    public Voyage createVoyage(String titre, String description) {
        return travelManager.createVoyage(titre,description);
    }
}
