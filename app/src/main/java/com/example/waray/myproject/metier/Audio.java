package com.example.waray.myproject.metier;

import java.util.Date;

/**
 * Created by Achrafpc on 03/12/2017.
 */

public class Audio {

    long id;
    String date;
    String url;
    double lat;
    double lng;
    long idVoyage;

    public Audio(Audio audio) {
        this.id = audio.id;
        this.date = audio.date;
        this.url = audio.url;
        this.lat = audio.lat;
        this.lng = audio.lng;
        this.idVoyage = audio.idVoyage;
    }

    public Audio(String date, String url, double lat, double lng, long idVoyage) {
        this.date = date;
        this.url = url;
        this.lat = lat;
        this.lng = lng;
        this.idVoyage = idVoyage;
    }

    public Audio() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(long idVoyage) {
        this.idVoyage = idVoyage;
    }
}
