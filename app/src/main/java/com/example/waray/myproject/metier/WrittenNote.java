package com.example.waray.myproject.metier;

/**
 * Created by Achrafpc on 16/12/2017.
 */

public class WrittenNote {

    long id;
    String date;
    double lat;
    double lng;
    String title;
    String description;
    long idVoyage;

    public WrittenNote( String date, double lat, double lng, String title, String description, long idVoyage) {
        this.date = date;
        this.lat = lat;
        this.lng = lng;
        this.title = title;
        this.description = description;
        this.idVoyage = idVoyage;
    }

    public WrittenNote() {
    }

    public WrittenNote(WrittenNote writtenNote) {
        this.id = writtenNote.id;
        this.date = writtenNote.date;
        this.lat = writtenNote.lat;
        this.lng = writtenNote.lng;
        this.title = writtenNote.title;
        this.description = writtenNote.description;
        this.idVoyage = writtenNote.idVoyage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(long idVoyage) {
        this.idVoyage = idVoyage;
    }
}
