package com.example.waray.myproject.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waray on 06/12/2017.
 */

public class VideoManagerDao implements VideoManagerItf {

    protected Context context;

    protected SQLiteDatabase database;

    private String[] allColumnsVideo = { MySQLiteHelper.COLUMN_Video_ID, MySQLiteHelper.COLUMN_Video_Name,
            MySQLiteHelper.COLUMN_Video_Url,MySQLiteHelper.COLUMN_Video_Lat,
            MySQLiteHelper.COLUMN_Video_Lng, MySQLiteHelper.COLUMN_Video_IdVoyage };

    public VideoManagerDao(Context context) {
        this.context = context;
        database = DAO.getInstance(context);
    }

    @Override
    public Video createVideo(String nom, long idVoyage, double lat, double lng, String url) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_Video_Name, nom);
        values.put(MySQLiteHelper.COLUMN_Video_Url, url);
        values.put(MySQLiteHelper.COLUMN_Video_Lat, lat);
        values.put(MySQLiteHelper.COLUMN_Video_Lng, lng);
        values.put(MySQLiteHelper.COLUMN_Video_IdVoyage, idVoyage);

        long insertId = database.insert(MySQLiteHelper.TABLE_Video, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_Video,
                allColumnsVideo, MySQLiteHelper.COLUMN_Video_ID+ " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Video newVideo = cursorToVideo(cursor);
        cursor.close();
        return newVideo;
    }

    @Override
    public void deleteVideo(Video video) {
        long id = video.getId();
        database.delete(MySQLiteHelper.TABLE_Video, MySQLiteHelper.COLUMN_Video_ID
                + " = " + id, null);
    }

    @Override
    public List<Video> getVideoByVoyage(Long idVoyage) {
        List<Video> videos  = new ArrayList<Video>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_Video,
                allColumnsVideo, null, null, null, null, MySQLiteHelper.COLUMN_Video_ID+"   DESC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Video video= cursorToVideo(cursor);
            if (video.getIdVoyage() == idVoyage){
                videos.add(video);
            }
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return videos;
    }

    private Video cursorToVideo(Cursor cursor) {
        Video video = new Video();
        video.setId(cursor.getLong(0));
        video.setNom(cursor.getString(1));
        video.setUrl(cursor.getString(2));
        video.setLat(cursor.getDouble(3));
        video.setLng(cursor.getDouble(4));
        video.setIdVoyage(cursor.getLong(5));
        return video;
    }
}
