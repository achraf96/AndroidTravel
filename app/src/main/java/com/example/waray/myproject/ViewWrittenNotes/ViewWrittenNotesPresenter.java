package com.example.waray.myproject.ViewWrittenNotes;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.WrittenNoteManagerItf;
import com.example.waray.myproject.metier.WrittenNote;

import java.util.List;

/**
 * Created by Achrafpc on 17/12/2017.
 */

public class ViewWrittenNotesPresenter implements ViewWrittenNotesPrensterItf {

    public ViewWrittenNotesActivityItf view;

    public WrittenNoteManagerItf writtenNoteManagerItf;

    public ViewWrittenNotesPresenter(ViewWrittenNotesActivityItf view) {
        this.view = view;
        writtenNoteManagerItf = AppManager.getInstance().getWrittenNoteManager();
    }

    @Override
    public List<WrittenNote> getWrittenNotesByVoyage(Long idVoyage) {
        return writtenNoteManagerItf.getWrittenNotesByVoyage(idVoyage);
    }

    @Override
    public void deleteWritenNote(WrittenNote writtenNote) {
        writtenNoteManagerItf.deleteWrittenNote(writtenNote);
    }
}
