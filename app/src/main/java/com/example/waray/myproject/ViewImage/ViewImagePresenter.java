package com.example.waray.myproject.ViewImage;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.ImageManagerItf;
import com.example.waray.myproject.metier.Image;

/**
 * Created by Waray on 20/12/2017.
 */

public class ViewImagePresenter implements ViewImagePresenterItf {

    protected ViewImageActivityItf view;

    protected ImageManagerItf imageManager ;

    public ViewImagePresenter(ViewImageActivity view) {

        this.view = view;
        imageManager = AppManager.getInstance().getImageManager();

    }

    @Override
    public void deleteImage(Image image) {
        imageManager.deleteImage(image);
    }
}
