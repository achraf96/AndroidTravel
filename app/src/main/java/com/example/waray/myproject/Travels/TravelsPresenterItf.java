package com.example.waray.myproject.Travels;

import android.arch.persistence.room.Delete;
import android.view.Menu;
import android.view.MenuItem;

import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Voyage;

import java.util.List;

/**
 * Created by Waray on 18/11/2017.
 */

public interface TravelsPresenterItf {

    public List<Voyage> getAllVoyages();

    public void deleteVoyage(Voyage voyage);

    public List<Image> getImagesByVoyage(Long idVoyage);

}
