package com.example.waray.myproject.ViewVideos;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.waray.myproject.Map.Map;
import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewImages.ViewImagesListAdapter;
import com.example.waray.myproject.ViewImages.ViewImagesPresenter;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class ViewVideosActivity extends AppCompatActivity implements ViewVideosActivityItf {

    private List<Video> videos = null;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Long IDVoyage;
    private Context context ;
    private ViewVideosPresenterItf videoPresenterItf;
    private GoogleMap mGoogleMap;
    private Map map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_videos_activity);
        context=getApplicationContext();
//        map = new Map(context, this);
//        if (map.googleServicesAvailable()) initMap();

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_videos);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        videoPresenterItf = new ViewVideosPresenter(this);

        IDVoyage = getIntent().getLongExtra("IDVOYAGE",0);

        videos = videoPresenterItf.getVideosByVoyage(IDVoyage);

        adapter = new ViewVideosListAdapter(videos,this.getApplicationContext());

        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder
                            target) {
                        return false;
                    }
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        final int position = viewHolder.getAdapterPosition(); //get position which is swipe

                        AlertDialog.Builder builder = new AlertDialog.Builder(recyclerView.getContext()); //alert for confirm to delete
                        builder.setMessage("Are you sure you want to delete?");    //set message

                        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position);    //travels_activity_item removed from recylcerview
                                new File(videos.get(position).getUrl()).delete();

                                videoPresenterItf.deleteVideo(videos.get(position));
                                videos.remove(position);  //then remove travels_activity_item

                                return;
                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position+1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                                adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                                return;
                            }
                        }).show();  //show alert dialog
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

//    @Override
//    public void initMap() {
//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragmentVideos);
//        mapFragment.getMapAsync(this);
//    }
//
//    @Override
//    public void setVideosMarkers(List<Video> videos, GoogleMap mGoogleMap) throws IOException {
//        Geocoder geocoder = new Geocoder(this);
//
//        Iterator<Video> iterator = videos.iterator();
//        while (iterator.hasNext()) {
//            Video video = iterator.next();
//            MarkerOptions options = new MarkerOptions()
//                    .title(geocoder.getFromLocation(video.getLat(), video.getLng(), 1).get(0).getLocality())
//                    .snippet("Video")
//                    .position(new LatLng(video.getLat(),video.getLng()));
//            mGoogleMap.addMarker(options);
//        }
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mGoogleMap = googleMap;
//        //goToLocationZoom(39.008224,-76.8984527, 15);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mGoogleMap.setMyLocationEnabled(true);
//        try {
//            setVideosMarkers(videos, mGoogleMap);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
