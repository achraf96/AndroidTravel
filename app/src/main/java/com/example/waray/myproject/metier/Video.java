package com.example.waray.myproject.metier;

/**
 * Created by Waray on 06/12/2017.
 */

public class Video {

    long id;
    String nom;
    String url;
    double lat;
    double lng;
    long idVoyage;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getIdVoyage() {
        return idVoyage;
    }

    public void setIdVoyage(long idVoyage) {
        this.idVoyage = idVoyage;
    }

    public Video(Video video) {
        this.id = video.id;
        this.nom = video.nom;
        this.url = video.url;
        this.lat = video.lat;
        this.lng = video.lng;
        this.idVoyage = video.idVoyage;
    }

    public Video(){
    }
}
