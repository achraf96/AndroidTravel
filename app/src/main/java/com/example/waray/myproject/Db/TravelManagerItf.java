package com.example.waray.myproject.Db;

import android.database.Cursor;

import com.example.waray.myproject.metier.Voyage;

import java.util.List;

/**
 * Created by Waray on 18/11/2017.
 */

public interface TravelManagerItf {

    Voyage createVoyage(String titre, String description);

    void deleteVoyage(Voyage voyage);

    List<Voyage> getAllVoyages();

    Voyage cursorToVoyage(Cursor cursor);

}
