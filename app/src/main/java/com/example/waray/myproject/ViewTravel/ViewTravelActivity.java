package com.example.waray.myproject.ViewTravel;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waray.myproject.NewWrittenNote.NewWrittenNoteActivity;
import com.example.waray.myproject.R;
import com.example.waray.myproject.Travels.TravelsListAdapter;
import com.example.waray.myproject.Util;
import com.example.waray.myproject.ViewAudio.ViewAudiosActivity;
import com.example.waray.myproject.ViewImages.ViewImagesActivity;
import com.example.waray.myproject.ViewNotesMap.ViewNotesMapActivity;
import com.example.waray.myproject.ViewWrittenNotes.ViewWrittenNotesActivity;
import com.example.waray.myproject.ViewVideos.ViewVideosActivity;
import com.github.clans.fab.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;

public class ViewTravelActivity extends AppCompatActivity implements ViewTravelActivityItf {
    private static final int REQUEST_RECORD_AUDIO = 0;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_TAKE_VIDEO = 2;
    FloatingActionButton buttonTakePicture;
    Button buttonViewPictures;
    FloatingActionButton buttonTakeVideo;
    Button buttonViewVideos;
    TextView textViewTitle;
    TextView textViewDescription;
    Long ID;
    String TITLE;
    String DESCRIPTION;
    double lat;
    double lng;
    String imageFileName;
    String videoFileName;
    String mCurrentPhotoPath;
    String mCurrentVideoPath;
    ViewTravelPresenterItf viewTravelPresenterItf;

    FloatingActionButton buttonRecorderAudio;
    public String audioFilepath;

    Button buttonViewAudios;
    FloatingActionButton buttonAddWrittenNote;
    android.support.design.widget.FloatingActionButton buttonMap;
    Button buttonViewWrittenNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_travel_activity);

        viewTravelPresenterItf = new ViewTravelPresenter(this);

        ID = getIntent().getLongExtra("IDVOYAGE",0);
        TITLE = getIntent().getStringExtra("TITLEVOYAGE");
        DESCRIPTION = getIntent().getStringExtra("DESCRIPTIONVOYAGE");
        textViewTitle = (TextView) findViewById(R.id.textViewTravelTitle);
        textViewDescription = (TextView) findViewById(R.id.textViewTravelDescription);
        buttonTakePicture =  findViewById(R.id.buttonTakePicture);
        buttonTakeVideo =  findViewById(R.id.buttonRecordVideo);
        buttonTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    dispatchTakePictureIntent();
                }
            }
        });

        buttonTakeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if(takeVideoIntent.resolveActivity(getPackageManager()) != null){
                    dispatchTakeVideoIntent();
                }
            }
        });

        textViewTitle.setText(TITLE);
        textViewDescription.setText(DESCRIPTION);

        buttonViewPictures = (Button) findViewById(R.id.buttonViewPictures);
        buttonViewPictures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentViewPictures = new Intent(getApplicationContext(),ViewImagesActivity.class);
                intentViewPictures.putExtra("IDVOYAGE",ID);
                startActivity(intentViewPictures);
            }
        });

        buttonViewVideos = (Button) findViewById(R.id.buttonViewVideos);
        buttonViewVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentViewVideos = new Intent(getApplicationContext(),ViewVideosActivity.class);
                intentViewVideos.putExtra("IDVOYAGE",ID);
                startActivity(intentViewVideos);
            }
        });

        buttonRecorderAudio =  findViewById(R.id.buttonAudioRecorder);

        buttonRecorderAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    recordAudio();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        Util.requestPermission(this, Manifest.permission.RECORD_AUDIO);
        Util.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        buttonViewAudios = (Button) findViewById(R.id.buttonViewAudios);

        buttonViewAudios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentViewAudios = new Intent(getApplicationContext(),ViewAudiosActivity.class);
                intentViewAudios.putExtra("IDVOYAGE",ID);
                startActivity(intentViewAudios);
            }
        });

        buttonAddWrittenNote =  findViewById(R.id.ButtonAddWrittenNote);

        buttonAddWrittenNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckUserPermsions();
                Intent intentAddWrittenNote = new Intent(getApplicationContext(), NewWrittenNoteActivity.class);
                Log.w("Lat"," "+lat);
                Log.w("lng"," "+lng);
                intentAddWrittenNote.putExtra("IDVOYAGE",ID);
                intentAddWrittenNote.putExtra("Lat",lat);
                intentAddWrittenNote.putExtra("Lng",lng);
                startActivity(intentAddWrittenNote);
            }
        });

        buttonViewWrittenNote = findViewById(R.id.ButtonViewWrittenNote);
        buttonViewWrittenNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentAddWrittenNote = new Intent(getApplicationContext(), ViewWrittenNotesActivity.class);
                intentAddWrittenNote.putExtra("IDVOYAGE",ID);
                startActivity(intentAddWrittenNote);
            }
        });


        buttonMap = findViewById(R.id.fabMap);
        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentAddWrittenNote = new Intent(getApplicationContext(), ViewNotesMapActivity.class);
                intentAddWrittenNote.putExtra("IDVOYAGE",ID);
                startActivity(intentAddWrittenNote);
            }
        });
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            // Create the file where the video should go
            File videoFile = null;
            try {
                videoFile = createVideoFile();
            } catch (IOException ex){
                // Error occurred while creating the File
                Toast toast = Toast.makeText(getApplicationContext(),"IOException : Video - ViewTravelActivity.java",Toast.LENGTH_SHORT);
                toast.show();
            }
            if (videoFile != null) {
                Uri videoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        videoFile);
                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoURI);
                startActivityForResult(takeVideoIntent, REQUEST_TAKE_VIDEO);

            }
        }


    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(getApplicationContext(),"IOException : Image - ViewTravelActivity.java",Toast.LENGTH_SHORT);
                toast.show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }

    private File createVideoFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("EEE_d_MMM_yyyy_HH:mm:ss").format(new Date());
        videoFileName = TITLE + "_" + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_DCIM); //Environment.getExternalStoragePublicDirectory
        File video = File.createTempFile(
                videoFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentVideoPath = video.getAbsolutePath();
        Toast.makeText(this,mCurrentVideoPath,Toast.LENGTH_LONG).show();
        return video;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("EEE d MMM yyyy HH:mm:ss").format(new Date());
        imageFileName = TITLE + " " + timeStamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES); //Environment.getExternalStoragePublicDirectory
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            CheckUserPermsions();
            viewTravelPresenterItf.createImage(imageFileName,ID,lat,lng,mCurrentPhotoPath);
        }
        if (requestCode == REQUEST_TAKE_VIDEO && resultCode == RESULT_OK) {
            CheckUserPermsions();
            Toast.makeText(this,"Nom :"+videoFileName,Toast.LENGTH_LONG).show();
            viewTravelPresenterItf.createVideo(videoFileName,ID,lat,lng,mCurrentVideoPath);
            //viewTravelPresenterItf.createImage(imageFileName,ID,lat,lng,mCurrentPhotoPath);
        }

        if (requestCode == REQUEST_RECORD_AUDIO) {
            if (resultCode == RESULT_OK) {
                //CheckUserPermsions();
                String timeStamp = new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(new Date());
                viewTravelPresenterItf.createAudio(timeStamp,ID,lat,lng,audioFilepath);
                Toast.makeText(this, audioFilepath+"  Audio recorded successfully!", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Audio was not recorded", Toast.LENGTH_SHORT).show();
            }
        }

    }

    //get acces to location permsion
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;


    void getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if(location!=null){
            lat = location.getLatitude();
            lng = location.getLongitude();
            //Toast.makeText(this, "Long : "+lng+" Lat : "+lat, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "No location found", Toast.LENGTH_LONG).show();
        }
    }

    //access to permsions
    void CheckUserPermsions(){
        if ( Build.VERSION.SDK_INT >= 23){
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED  ){
                requestPermissions(new String[]{
                                android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return ;
            }
        }

        getLocation();// init the contact list

    }

    public void recordAudio() throws IOException {

        audioFilepath = creatAudioFile();
        Toast.makeText(getApplicationContext(),audioFilepath,Toast.LENGTH_LONG).show();
        AndroidAudioRecorder.with(this)
                // Required
                .setFilePath(audioFilepath)
                .setColor(ContextCompat.getColor(this, R.color.recorder_bg))
                .setRequestCode(REQUEST_RECORD_AUDIO)

                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(false)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
    }

    public String creatAudioFile() throws IOException {
        //creation lien de l'image

        String timeStamp= new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String audioFileName="recorded_audio_"+timeStamp+"_";

        File storageDir= getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC);

        File audio=File.createTempFile(
                audioFileName,
                ".wav",
                storageDir
        );


        return audio.getAbsolutePath();
    }



}
