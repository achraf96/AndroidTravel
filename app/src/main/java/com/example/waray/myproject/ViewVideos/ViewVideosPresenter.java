package com.example.waray.myproject.ViewVideos;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.VideoManagerItf;
import com.example.waray.myproject.ViewImages.ViewImagesActivity;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

import java.util.List;

/**
 * Created by Waray on 07/12/2017.
 */

public class ViewVideosPresenter implements ViewVideosPresenterItf {

    protected ViewVideosActivityItf view;

    protected VideoManagerItf videoManager ;

    public ViewVideosPresenter(ViewVideosActivity view) {

        this.view = view;
        videoManager = AppManager.getInstance().getVideoManager();

    }

    @Override
    public Video createVideo(String nom, long idVoyage, double lat, double lng, String url) {
        return videoManager.createVideo(nom,idVoyage, lat, lng, url);
    }

    @Override
    public void deleteVideo(Video video) {
        videoManager.deleteVideo(video);
    }

    @Override
    public List<Video> getVideosByVoyage(Long idVoyage) {

        return videoManager.getVideoByVoyage(idVoyage);
    }
}
