package com.example.waray.myproject.ViewVideos;

import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewImages.ViewImagesListAdapter;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

import java.util.List;

/**
 * Created by Waray on 07/12/2017.
 */

public class ViewVideosListAdapter extends RecyclerView.Adapter<ViewVideosListAdapter.ViewHolder> {

    private List<Video> values;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public TextView textViewLat;
        public TextView textViewLng;
        public VideoView videoViewVideo;
        public View layout;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = (TextView) view.findViewById(R.id.textViewVideoName);
            this.textViewLat = (TextView) view.findViewById(R.id.textViewVideoLat);
            this.textViewLng = (TextView) view.findViewById(R.id.textViewVideoLng);
            this.videoViewVideo  = (VideoView) view.findViewById(R.id.videoViewVideo);
            this.layout = view;
        }
    }

    public void add(int position, Video video){
        values.add(position, video);
        notifyItemInserted(position);
    }

    public void remove(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }

    public ViewVideosListAdapter(List<Video> myDataset, Context context) {
        values = myDataset;
        this.context=context;
    }

    @Override
    public ViewVideosListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_videos_item, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewVideosListAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Video video = new Video(values.get(position));
        Toast.makeText(context,"Here : "+video.getUrl(),Toast.LENGTH_LONG).show();
        final String name = video.getNom();
        final String url = video.getUrl();
        final double lat = video.getLat();
        final double lng = video.getLng();

        holder.textViewName.setText(name);
        holder.textViewLat.setText(String.valueOf(lat));
        holder.textViewLng.setText(String.valueOf(lng));


        //holder.videoViewVideo.setVideoURI(Uri.parse("android.resource://"+url));

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.videoViewVideo.isPlaying())
                    holder.videoViewVideo.pause();
                else
                    holder.videoViewVideo.start();
            }
        });

        holder.videoViewVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // This is just to show image when loaded
                mp.start();
                mp.pause();
            }
        });

        holder.videoViewVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // setLooping(true) didn't work, thats why this workaround
                holder.videoViewVideo.setVideoURI(Uri.parse(url));
                //holder.videoViewVideo.start();
            }
        });


        holder.videoViewVideo.setVideoURI(Uri.parse(url));


    }

    @Override
    public int getItemCount() {
        return values.size();
    }

}
