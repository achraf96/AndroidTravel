package com.example.waray.myproject.ViewAudio;

import android.media.AudioManager;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.AudioManagerItf;
import com.example.waray.myproject.metier.Audio;

import java.util.Date;
import java.util.List;

/**
 * Created by Achrafpc on 03/12/2017.
 */

public class ViewAudiosPresenter implements ViewAudiosPresenterItf {

    protected ViewAudiosActivityItf view;
    protected AudioManagerItf audioManager;


    public ViewAudiosPresenter(ViewAudiosActivityItf view) {
        this.view = view;
        this.audioManager = AppManager.getInstance().getAudioManager();
    }


    @Override
    public Audio createAudio(String date, long idVoyage, double lat, double lng, String url) {
        return this.audioManager.createAudio(date,idVoyage,lat,lng,url);
    }

    @Override
    public void deleteAudio(Audio audio) {
        this.audioManager.deleteAudio(audio);
    }

    @Override
    public List<Audio> getAudiosByVoyage(Long idVoyage) {
       return this.audioManager.getAudiosByVoyage(idVoyage);
    }
}
