package com.example.waray.myproject.NewWrittenNote;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.WrittenNoteManagerItf;
import com.example.waray.myproject.metier.WrittenNote;

/**
 * Created by Achrafpc on 16/12/2017.
 */

public class NewWrittenNotePresenter implements NewWrittenNotePresenterItf{

    protected NewWrittenNoteActivityItf view;

    protected WrittenNoteManagerItf writtenNoteManagerItf;

    public NewWrittenNotePresenter(NewWrittenNoteActivityItf view) {
        this.view = view;
        writtenNoteManagerItf = AppManager.getInstance().getWrittenNoteManager();
    }

    @Override
    public WrittenNote createWrittenNote(String date, double lat, double lng, String title, String description, long idVoyage) {
        return writtenNoteManagerItf.createWrittenNote( date, lat,  lng,  title,  description,  idVoyage);
    }
}
