package com.example.waray.myproject.ViewAudio;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.TypedValue;

import com.example.waray.myproject.GridSpacingItemDecoration;
import com.example.waray.myproject.R;
import com.example.waray.myproject.metier.Audio;

import java.io.File;
import java.util.List;

public class ViewAudiosActivity extends AppCompatActivity implements ViewAudiosActivityItf{

    private List<Audio> audios = null;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Long IDVoyage;
    private Context context ;
    private ViewAudiosPresenterItf audioPresenterItf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_audios_activity);

        context = getApplicationContext();
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_audios);
        layoutManager = new GridLayoutManager(this,4);
        recyclerView.setLayoutManager(layoutManager);

        audioPresenterItf = new ViewAudiosPresenter(this);

        IDVoyage = getIntent().getLongExtra("IDVOYAGE",0);

        audios = audioPresenterItf.getAudiosByVoyage(IDVoyage);

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(4, dpToPx(4), true));
        recyclerView.setAdapter(adapter);


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder
                            target) {
                        return false;
                    }
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        final int position = viewHolder.getAdapterPosition(); //get position which is swipe

                        AlertDialog.Builder builder = new AlertDialog.Builder(recyclerView.getContext()); //alert for confirm to delete
                        builder.setMessage("Are you sure you want to delete?");    //set message

                        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position);    //travels_activity_item removed from recylcerview
                                new File(audios.get(position).getUrl()).delete();

                                audioPresenterItf.deleteAudio(audios.get(position));
                                audios.remove(position);  //then remove travels_activity_item

                                return;
                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position+1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                                adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                                return;
                            }
                        }).show();  //show alert dialog
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    @Override
    protected void onResume() {
        audios = audioPresenterItf.getAudiosByVoyage(IDVoyage);
        adapter = new ViewAudioListAdapter(audios,this);
        recyclerView.setAdapter(adapter);
        super.onResume();
    }
}
