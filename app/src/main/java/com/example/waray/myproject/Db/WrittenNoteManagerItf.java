package com.example.waray.myproject.Db;

import com.example.waray.myproject.metier.WrittenNote;

import java.util.List;

/**
 * Created by Achrafpc on 16/12/2017.
 */

public interface WrittenNoteManagerItf {

    public WrittenNote createWrittenNote(String date, double lat, double lng, String title, String description, long idVoyage) ;
    public void deleteWrittenNote(WrittenNote writtenNote);
    public List<WrittenNote> getWrittenNotesByVoyage(Long idVoyage);
}
