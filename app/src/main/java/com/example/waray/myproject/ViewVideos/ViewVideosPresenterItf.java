package com.example.waray.myproject.ViewVideos;

import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

import java.util.List;

/**
 * Created by Waray on 07/12/2017.
 */

public interface ViewVideosPresenterItf {

    public Video createVideo(String nom, long idVoyage, double lat, double lng, String url) ;
    public void deleteVideo(Video video);
    public List<Video> getVideosByVoyage(Long idVoyage);
}
