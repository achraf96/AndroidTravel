package com.example.waray.myproject.Map;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewImages.ViewImagesActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

/**
 * Created by Waray on 03/12/2017.
 */

public class Map {

    Context context;
    Activity ActivityToOpen;
    GoogleMap mGoogleMap;

    public Map(Context context, Activity activityToOpen) {
        this.context = context;
        ActivityToOpen = activityToOpen;
    }

    public Boolean googleServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int isAvailabe = apiAvailability.isGooglePlayServicesAvailable(context);
        if (isAvailabe == ConnectionResult.SUCCESS) {
            return true;
        } else if (apiAvailability.isUserResolvableError(isAvailabe)) {
            Dialog dialog = apiAvailability.getErrorDialog(ActivityToOpen, isAvailabe, 0);
            dialog.show();
        } else {
            Toast.makeText(context, "Cant connect to Google services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void goToLocation(double lat, double lng) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLng(ll);
        mGoogleMap.moveCamera(update);
    }

    public void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mGoogleMap.moveCamera(update);
    }

//    public void geoLocate(View view) throws IOException {
//        EditText et = (EditText) view.findViewById(R.id.editText);
//        String location = et.getText().toString();
//
//        Geocoder geocoder = new Geocoder(context);
//        List<Address> list = geocoder.getFromLocationName(location, 1);
//        Address address = list.get(0);
//        String locality = address.getLocality();
//
//        Toast.makeText(context, locality, Toast.LENGTH_LONG).show();
//
//        double lat = address.getLatitude();
//        double lng = address.getLongitude();
//
//        goToLocationZoom(lat, lng, 14);
//
//        MarkerOptions options = new MarkerOptions()
//                .title(locality)
//                .snippet("Picture")
//                .position(new LatLng(lat,lng));
//        mGoogleMap.addMarker(options);
//
//    }

}
