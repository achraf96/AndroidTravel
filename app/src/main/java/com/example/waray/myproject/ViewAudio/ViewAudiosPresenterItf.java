package com.example.waray.myproject.ViewAudio;

import com.example.waray.myproject.metier.Audio;

import java.util.Date;
import java.util.List;

/**
 * Created by Achrafpc on 03/12/2017.
 */

interface ViewAudiosPresenterItf {

    public Audio createAudio(String date, long idVoyage, double lat, double lng, String url) ;
    public void deleteAudio(Audio audio);
    public List<Audio> getAudiosByVoyage(Long idVoyage);
}
