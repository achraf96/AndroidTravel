package com.example.waray.myproject.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.waray.myproject.metier.Audio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Achrafpc on 03/12/2017.
 */

public class AudioManagerDao implements AudioManagerItf{


    protected Context context;

    protected SQLiteDatabase database;

    private String[] allColumnsAudio = { MySQLiteHelper.COLUMN_Audio_ID, MySQLiteHelper.COLUMN_Audio_DATE,
            MySQLiteHelper.COLUMN_Audio_Url,MySQLiteHelper.COLUMN_Audio_Lat,
            MySQLiteHelper.COLUMN_Audio_Lng, MySQLiteHelper.COLUMN_Audio_IdVoyage };


    public AudioManagerDao(Context context) {
        this.context = context;
        database = DAO.getInstance(context);
    }

    public Audio createAudio(String date, long idVoyage, double lat, double lng, String url) {

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_Audio_DATE, date);
        values.put(MySQLiteHelper.COLUMN_Audio_Url, url);
        values.put(MySQLiteHelper.COLUMN_Audio_Lat, lat);
        values.put(MySQLiteHelper.COLUMN_Audio_Lng, lng);
        values.put(MySQLiteHelper.COLUMN_Audio_IdVoyage, idVoyage);

        long insertId = database.insert(MySQLiteHelper.TABLE_Audio, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_Audio,
                allColumnsAudio, MySQLiteHelper.COLUMN_Audio_ID+ " = " + insertId, null,
                null, null, MySQLiteHelper.COLUMN_Audio_ID+" DESC");
        cursor.moveToFirst();
        Audio newAudio = cursorToAudio(cursor);
        cursor.close();
        return newAudio;
    }

    public void deleteAudio(Audio audio) {
        long id = audio.getId();
        database.delete(MySQLiteHelper.TABLE_Audio, MySQLiteHelper.COLUMN_Audio_ID
                + " = " + id, null);
    }

    public List<Audio> getAudiosByVoyage(Long idVoyage) {
        List<Audio> audios = new ArrayList<Audio>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_Audio,
                allColumnsAudio, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Audio audio= cursorToAudio(cursor);
            if (audio.getIdVoyage() == idVoyage){
                audios.add(audio);
            }
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return audios;
    }


    private Audio cursorToAudio(Cursor cursor) {
        Audio audio = new Audio();
        audio.setId(cursor.getLong(0));
        audio.setDate(cursor.getString(1));
        audio.setUrl(cursor.getString(2));
        audio.setLat(cursor.getDouble(3));
        audio.setLng(cursor.getDouble(4));
        audio.setIdVoyage(cursor.getLong(5));
        return audio;
    }
}
