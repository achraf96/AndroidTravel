package com.example.waray.myproject.NewTravel;

import com.example.waray.myproject.metier.Voyage;

/**
 * Created by Waray on 18/11/2017.
 */

public interface NewTravelPresenterItf {

    public Voyage createVoyage(String titre, String description);

}
