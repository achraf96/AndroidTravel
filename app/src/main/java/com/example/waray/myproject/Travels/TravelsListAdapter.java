package com.example.waray.myproject.Travels;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewTravel.ViewTravelActivity;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Voyage;

import java.util.List;

/**
 * Created by Waray on 12/10/2017.
 */

public class TravelsListAdapter extends RecyclerView.Adapter<TravelsListAdapter.ViewHolder> {
    private List<Voyage> values;
    private Context context;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public TextView textViewTitle;
        public TextView textViewDescription;
        public ImageView imageViewIcon;
        public View layout;

        public ViewHolder(View view) {
            super(view);
            this.textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            this.textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);
            this.cardView = (CardView) view.findViewById(R.id.card_view_travel);
            this.imageViewIcon = (ImageView) view.findViewById(R.id.imageViewCardIcon);
            this.layout = view;
        }
    }

        public void add(int position, Voyage voyage){
            values.add(position, voyage);
            notifyItemInserted(position);
        }

        public void remove(int position){
            values.remove(position);
            notifyItemRemoved(position);
        }

        public TravelsListAdapter(List<Voyage> myDataset, Context context) {
            values = myDataset;
            this.context=context;
        }

        @Override
        public TravelsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View v = inflater.inflate(R.layout.travels_activity_item, parent, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            // set the view's size, margins, paddings and layout parameters
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Voyage voyage = new Voyage(values.get(position));
        final String title = values.get(position).getTitre();
        final String description = values.get(position).getDescription();



        if(position % 6 == 0){
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.card_view_item_1));
            holder.imageViewIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_card_1));

        }else if(position % 6 == 1){
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.card_view_item_2));
            holder.textViewDescription.setTextColor(ContextCompat.getColor(context,R.color.text_color_2));
            holder.textViewTitle.setTextColor(ContextCompat.getColor(context,R.color.text_color_2));
            holder.imageViewIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_card_3));
        }else if(position % 6 == 2){
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.card_view_item_3));
            holder.imageViewIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_card_4));
        }else if(position % 6 == 3){
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.card_view_item_4));
            holder.imageViewIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_card_6));
        }else if(position % 6 == 4){
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.card_view_item_5));
            holder.imageViewIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_card_7));
        } else if(position % 6 == 5){
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context,R.color.card_view_item_6));
            holder.textViewDescription.setTextColor(ContextCompat.getColor(context,R.color.text_color_2));
            holder.textViewTitle.setTextColor(ContextCompat.getColor(context,R.color.text_color_2));
            holder.imageViewIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_card_5));
        }

        holder.textViewTitle.setText(title);
//        if(description.length()>=20)
//            holder.textViewTitle.setText(title.substring(0,19)+"...");
//        else
//            holder.textViewTitle.setText(title);
//
//        if(description.length()>=130)
//            holder.textViewDescription.setText(description.substring(0,129)+"...");
//        else
        holder.textViewDescription.setText(description);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ViewTravelActivity.class);
                intent.putExtra("IDVOYAGE",voyage.getId());
                intent.putExtra("TITLEVOYAGE",voyage.getTitre());
                intent.putExtra("DESCRIPTIONVOYAGE",voyage.getDescription());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

    }



        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return values.size();
        }


}

