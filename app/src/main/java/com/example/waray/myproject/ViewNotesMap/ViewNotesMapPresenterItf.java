package com.example.waray.myproject.ViewNotesMap;

import com.example.waray.myproject.metier.Audio;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;
import com.example.waray.myproject.metier.WrittenNote;

import java.util.List;

/**
 * Created by Achrafpc on 21/12/2017.
 */

public interface ViewNotesMapPresenterItf {

    public List<Image> getImagesByVoyage(Long idVoyage);
    public List<Audio> getAudiosByVoyage(Long idVoyage);
    public List<Video> getVideosByVoyage(Long idVoyage);
    public List<WrittenNote> getWrittenNotesByVoyage(Long idVoyage);

}
