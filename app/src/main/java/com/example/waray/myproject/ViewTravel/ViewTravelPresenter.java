package com.example.waray.myproject.ViewTravel;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.AudioManagerItf;
import com.example.waray.myproject.Db.ImageManagerItf;
import com.example.waray.myproject.Db.VideoManagerItf;
import com.example.waray.myproject.metier.Audio;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

/**
 * Created by smlali on 21/11/17.
 */

public class ViewTravelPresenter implements ViewTravelPresenterItf {

    protected ViewTravelActivityItf view;

    protected ImageManagerItf imageManager;
    protected VideoManagerItf videoManager;

    protected AudioManagerItf audioManager;


    public ViewTravelPresenter(ViewTravelActivityItf view) {
        this.view = view;
        imageManager = AppManager.getInstance().getImageManager();
        videoManager = AppManager.getInstance().getVideoManager();
        audioManager = AppManager.getInstance().getAudioManager();
    }


    @Override
    public Image createImage(String nom, long idVoyage, double lat, double lng, String url) {
        return imageManager.createImage(nom,idVoyage,lat,lng,url);
    }

    @Override
    public Video createVideo(String nom, long idVoyage, double lat, double lng, String url) {
        return videoManager.createVideo(nom, idVoyage, lat, lng, url);
    }
    public Audio createAudio(String date, long idVoyage, double lat, double lng, String url) {
        return audioManager.createAudio(date,idVoyage,lat,lng,url);
    }


}
