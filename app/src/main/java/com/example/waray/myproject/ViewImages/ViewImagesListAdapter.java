package com.example.waray.myproject.ViewImages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waray.myproject.Db.DAO;
import com.example.waray.myproject.NewTravel.NewTravelActivity;
import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewImage.ViewImageActivity;
import com.example.waray.myproject.metier.Image;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Waray on 05/11/2017.
 */

public class ViewImagesListAdapter extends RecyclerView.Adapter<ViewImagesListAdapter.ViewHolder> {
    private List<Image> values;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewName;
        public TextView textViewLat;
        public TextView textViewLng;
        public ImageView imageViewImage;
        public View layout;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = (TextView) view.findViewById(R.id.textViewName2);
            this.textViewLat = (TextView) view.findViewById(R.id.textViewLat2);
            this.textViewLng = (TextView) view.findViewById(R.id.textViewLng2);
            this.imageViewImage = (ImageView) view.findViewById(R.id.imageViewImage2);
            this.layout = view;
        }
    }

    public void add(int position, Image image){
        values.add(position, image);
        notifyItemInserted(position);
    }

    public void remove(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }

    public ViewImagesListAdapter(List<Image> myDataset, Context context) {
        values = myDataset;
        this.context=context;
    }

    @Override
    public ViewImagesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_images_item2, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewImagesListAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Image image = new Image(values.get(position));
        final String name = image.getNom();
        final String url = image.getUrl();
        final double lat = image.getLat();
        final double lng = image.getLng();

        holder.textViewName.setText(name);
        holder.textViewLat.setText(String.valueOf(lat));
        holder.textViewLng.setText(String.valueOf(lng));
        //setPic(url, holder);
        holder.imageViewImage.setImageBitmap(decodeSampledBitmapFromFile(url,500, 500));

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context, "Simple click on "+image.getNom(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context, ViewImageActivity.class);
                i.putExtra("Image", image);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                return;
            }
        });

        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Toast.makeText(context, "Long click on "+image.getNom(),Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }

        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }


        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

}
