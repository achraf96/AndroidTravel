package com.example.waray.myproject.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.waray.myproject.metier.WrittenNote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Achrafpc on 16/12/2017.
 */

public class WrittenNoteManagerDao implements  WrittenNoteManagerItf{

    protected Context context;

    protected SQLiteDatabase database;

    private String[] allColumnsWrittenNote = { MySQLiteHelper.COLUMN_WrittenNote_ID, MySQLiteHelper.COLUMN_WrittenNote_DATE,
            MySQLiteHelper.COLUMN_WrittenNote_Lat,MySQLiteHelper.COLUMN_WrittenNote_Lng,
            MySQLiteHelper.COLUMN_WrittenNote_Title, MySQLiteHelper.COLUMN_WrittenNote_Description,
            MySQLiteHelper.COLUMN_WrittenNote_IdVoyage};


    public WrittenNoteManagerDao(Context context) {
        this.context = context;
        database = DAO.getInstance(context);
    }

    @Override
    public WrittenNote createWrittenNote(String date, double lat, double lng, String title, String description, long idVoyage) {

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_WrittenNote_DATE, date);
        values.put(MySQLiteHelper.COLUMN_WrittenNote_Lat, lat);
        values.put(MySQLiteHelper.COLUMN_WrittenNote_Lng, lng);
        values.put(MySQLiteHelper.COLUMN_WrittenNote_Title, title);
        values.put(MySQLiteHelper.COLUMN_WrittenNote_Description, description);
        values.put(MySQLiteHelper.COLUMN_WrittenNote_IdVoyage, idVoyage);

        long insertId = database.insert(MySQLiteHelper.TABLE_WrittenNote, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_WrittenNote,
                allColumnsWrittenNote, MySQLiteHelper.COLUMN_WrittenNote_ID+ " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        WrittenNote newWrittenNote = cursorToWrittenNote(cursor);
        cursor.close();
        return newWrittenNote;
    }

    @Override
    public void deleteWrittenNote(WrittenNote writtenNote) {
        long id = writtenNote.getId();
        database.delete(MySQLiteHelper.TABLE_WrittenNote, MySQLiteHelper.COLUMN_WrittenNote_ID
                + " = " + id, null);
    }

    @Override
    public List<WrittenNote> getWrittenNotesByVoyage(Long idVoyage) {
        List<WrittenNote> writtenNotes = new ArrayList<WrittenNote>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_WrittenNote,
                allColumnsWrittenNote, null, null, null, null, MySQLiteHelper.COLUMN_WrittenNote_ID+" DESC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            WrittenNote writtenNote= cursorToWrittenNote(cursor);
            if (writtenNote.getIdVoyage() == idVoyage){
                writtenNotes.add(writtenNote);
            }
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return writtenNotes;
    }

    private WrittenNote cursorToWrittenNote(Cursor cursor) {
        WrittenNote audio = new WrittenNote();
        audio.setId(cursor.getLong(0));
        audio.setDate(cursor.getString(1));
        audio.setLat(cursor.getDouble(2));
        audio.setLng(cursor.getDouble(3));
        audio.setTitle(cursor.getString(4));
        audio.setDescription(cursor.getString(5));
        audio.setIdVoyage(cursor.getLong(6));
        return audio;
    }
}


