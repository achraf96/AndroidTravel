package com.example.waray.myproject.ViewWrittenNotes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.waray.myproject.R;
import com.example.waray.myproject.Travels.TravelsListAdapter;
import com.example.waray.myproject.metier.WrittenNote;

import java.util.List;

/**
 * Created by Achrafpc on 17/12/2017.
 */

public class ViewWrittenNotesListAdapter extends RecyclerView.Adapter<ViewWrittenNotesListAdapter.ViewHolder>{

    private List<WrittenNote> values;
    private Context context;

    public ViewWrittenNotesListAdapter(List<WrittenNote> values, Context context) {
        this.values = values;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewTitle;
        public TextView textViewDescription;
        public View layout;

        public ViewHolder( View layout) {
            super(layout);
            this.textViewTitle = (TextView) layout.findViewById(R.id.textViewWrittenNoteTitle);
            this.textViewDescription =(TextView) layout.findViewById(R.id.textViewWrittenNoteDescription);
            this.layout = layout;
        }

    }

    public void add(int position, WrittenNote writtenNote){
        values.add(position, writtenNote);
        notifyItemInserted(position);
    }

    public void remove(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.written_note_activity_item, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        // set the view's size, margins, paddings and layout parameters
        ViewWrittenNotesListAdapter.ViewHolder vh = new ViewWrittenNotesListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final WrittenNote writtenNote = new WrittenNote(values.get(position));
        final String title = values.get(position).getTitle();
        final String description = values.get(position).getDescription();

        holder.textViewTitle.setText(title);
        holder.textViewDescription.setText(description);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


}
