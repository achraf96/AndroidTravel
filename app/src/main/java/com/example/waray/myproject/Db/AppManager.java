package com.example.waray.myproject.Db;

import android.app.Application;

import com.example.waray.myproject.metier.WrittenNote;

/**
 * Created by Waray on 18/11/2017.
 */

public class AppManager extends Application {

    static public AppManager instance;

    protected TravelManagerItf travelManager;
    protected ImageManagerItf imageManager;
    protected VideoManagerItf videoManager;
    protected AudioManagerItf audioManager;
    protected WrittenNoteManagerItf writtenNoteManager;


    static public AppManager getInstance(){
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("AppManager Created !");

        init();
    }

    private void init(){
        travelManager = new TravelManagerDao(this);
        imageManager = new ImageManagerDao(this);
        videoManager = new VideoManagerDao(this);
        audioManager = new AudioManagerDao(this);
        writtenNoteManager = new WrittenNoteManagerDao(this);
        instance = this;
    }

    public TravelManagerItf getTravelManager(){
        return travelManager;
    }
    public ImageManagerItf getImageManager(){return imageManager;}
    public VideoManagerItf getVideoManager(){return videoManager;}
    public AudioManagerItf getAudioManager(){return audioManager;}
    public WrittenNoteManagerItf getWrittenNoteManager(){return writtenNoteManager;}
}
