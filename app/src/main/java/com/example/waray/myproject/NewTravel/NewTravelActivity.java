package com.example.waray.myproject.NewTravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.waray.myproject.Db.DAO;
import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewTravel.ViewTravelActivity;
import com.example.waray.myproject.metier.Voyage;

public class NewTravelActivity extends AppCompatActivity implements NewTravelActivityItf {

    private DAO dao;
    private NewTravelPresenterItf newTravelPresenter;
    private Voyage voyage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_travel_activity);

        //dao = new DAO(this);
        //dao.open();

        newTravelPresenter = new NewTravelPresenter(this);

        final EditText editTextTitle = (EditText) findViewById(R.id.editTextTitle);
        final EditText editTextDescription = (EditText) findViewById(R.id.editTextDescription);

        Button buttonStartTravel = (Button) findViewById(R.id.buttonStartTravel);
        buttonStartTravel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TextUtils.isEmpty(editTextTitle.getText().toString()) || TextUtils.isEmpty(editTextDescription.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Please enter both title and description!",Toast.LENGTH_LONG).show();
                }
                else {
                    voyage = newTravelPresenter.createVoyage(editTextTitle.getText().toString(),editTextDescription.getText().toString());
                    //dao.createVoyage(editTextTitle.getText().toString(),editTextDescription.getText().toString());
                    //Toast.makeText(getApplicationContext(), "ViewTravelActivity added!",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(),ViewTravelActivity.class);
                    intent.putExtra("IDVOYAGE",voyage.getId());
                    intent.putExtra("TITLEVOYAGE",editTextTitle.getText().toString());
                    intent.putExtra("DESCRIPTIONVOYAGE",editTextDescription.getText().toString());
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onResume() {
        //dao.open();
        super.onResume();
    }

    @Override
    public void onPause() {
        //dao.close();
        super.onPause();
    }

}
