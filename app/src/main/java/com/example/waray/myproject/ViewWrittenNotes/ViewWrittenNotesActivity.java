package com.example.waray.myproject.ViewWrittenNotes;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.waray.myproject.R;
import com.example.waray.myproject.ViewAudio.ViewAudioListAdapter;
import com.example.waray.myproject.metier.WrittenNote;

import java.io.File;
import java.util.List;

public class ViewWrittenNotesActivity extends AppCompatActivity implements ViewWrittenNotesActivityItf{


    private List<WrittenNote> writtenNotes = null;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Long IDVoyage;
    private Context context ;
    private ViewWrittenNotesPrensterItf writtenNotesPrensterItf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_written_note);

        context = getApplicationContext();
        recyclerView = findViewById(R.id.my_recycler_view_WrittenNotes);
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        writtenNotesPrensterItf = new ViewWrittenNotesPresenter(this);

        IDVoyage = getIntent().getLongExtra("IDVOYAGE",0);

        writtenNotes = writtenNotesPrensterItf.getWrittenNotesByVoyage(IDVoyage);

        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder
                            target) {
                        return false;
                    }
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        final int position = viewHolder.getAdapterPosition(); //get position which is swipe

                        AlertDialog.Builder builder = new AlertDialog.Builder(recyclerView.getContext()); //alert for confirm to delete
                        builder.setMessage("Are you sure you want to delete?");    //set message

                        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position);    //travels_activity_item removed from recylcerview

                                writtenNotesPrensterItf.deleteWritenNote(writtenNotes.get(position));
                                writtenNotes.remove(position);  //then remove travels_activity_item

                                return;
                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position+1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                                adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                                return;
                            }
                        }).show();  //show alert dialog
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onResume() {
        writtenNotes = writtenNotesPrensterItf.getWrittenNotesByVoyage(IDVoyage);
        adapter = new ViewWrittenNotesListAdapter(writtenNotes,this);
        recyclerView.setAdapter(adapter);
        super.onResume();
    }
}
