package com.example.waray.myproject.Db;

import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

import java.util.List;

/**
 * Created by Waray on 06/12/2017.
 */

public interface VideoManagerItf {

    public Video createVideo(String nom, long idVoyage, double lat, double lng, String url) ;
    public void deleteVideo(Video video);
    public List<Video> getVideoByVoyage(Long idVoyage);

}
