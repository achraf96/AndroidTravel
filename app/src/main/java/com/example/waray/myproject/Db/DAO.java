package com.example.waray.myproject.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Voyage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waray on 11/10/2017.
 */

public class DAO {

    private static DAO daoInstance;

    private static SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    public static synchronized SQLiteDatabase getInstance(Context context){
        if(daoInstance == null){
            daoInstance = new DAO(context);
            daoInstance.open();
        }
        return database;
    }

    public DAO(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }


    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }


    public void close() {

        dbHelper.close();
    }


}
