package com.example.waray.myproject.ViewImages;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.waray.myproject.Db.DAO;
import com.example.waray.myproject.GridSpacingItemDecoration;
import com.example.waray.myproject.Map.Map;
import com.example.waray.myproject.R;
import com.example.waray.myproject.metier.Image;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class ViewImagesActivity extends AppCompatActivity implements ViewImagesActivityItf {

    private List<Image> images = null;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Long IDVoyage;
    private Context context ;
    private ViewImagesPresenterItf imagePresenterItf;
    private GoogleMap mGoogleMap;
    private Map map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_images_activity2);
        context=getApplicationContext();
//        map = new Map(context, this);
//        if (map.googleServicesAvailable()) initMap();


        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view_images2);

        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);


        imagePresenterItf = new ViewImagesPresenter(this);

        IDVoyage = getIntent().getLongExtra("IDVOYAGE",0);

        images = imagePresenterItf.getImagesByVoyage(IDVoyage);

        adapter = new ViewImagesListAdapter(images,this.getApplicationContext());

        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(6), true));
        recyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder
                            target) {
                        return false;
                    }
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        final int position = viewHolder.getAdapterPosition(); //get position which is swipe

                        AlertDialog.Builder builder = new AlertDialog.Builder(recyclerView.getContext()); //alert for confirm to delete
                        builder.setMessage("Are you sure you want to delete?");    //set message

                        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position);    //travels_activity_item removed from recylcerview
                                new File(images.get(position).getUrl()).delete();

                                imagePresenterItf.deleteImage(images.get(position));
                                images.remove(position);  //then remove travels_activity_item

                                return;
                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position+1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                                adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                                return;
                            }
                        }).show();  //show alert dialog
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    @Override
    protected void onResume() {
        images = imagePresenterItf.getImagesByVoyage(IDVoyage);
        adapter = new ViewImagesListAdapter(images,getApplicationContext());
        recyclerView.setAdapter(adapter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

//    public void initMap() {
//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment22);
//        mapFragment.getMapAsync(this);
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mGoogleMap = googleMap;
//        //goToLocationZoom(39.008224,-76.8984527, 15);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mGoogleMap.setMyLocationEnabled(true);
//        try {
//            setImagesMarkers(images, mGoogleMap);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    @Override
//    public void setImagesMarkers(List<Image> images, GoogleMap mGoogleMap) throws IOException {
//        Geocoder geocoder = new Geocoder(this);
//
//        Iterator<Image> iterator = images.iterator();
//        while (iterator.hasNext()) {
//            Image image = iterator.next();
//            MarkerOptions options = new MarkerOptions()
//                    .title(geocoder.getFromLocation(image.getLat(), image.getLng(), 1).get(0).getLocality())
//                    .snippet("Picture")
//                    .position(new LatLng(image.getLat(),image.getLng()));
//            mGoogleMap.addMarker(options);
//        }
//
//    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
