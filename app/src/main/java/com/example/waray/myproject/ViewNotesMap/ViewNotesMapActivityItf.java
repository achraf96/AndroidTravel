package com.example.waray.myproject.ViewNotesMap;

import com.example.waray.myproject.metier.Audio;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;
import com.example.waray.myproject.metier.WrittenNote;
import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;
import java.util.List;

/**
 * Created by Achrafpc on 21/12/2017.
 */

public interface ViewNotesMapActivityItf {
    public void setImagesMarkers(List<Image> images, GoogleMap mGoogleMap) throws IOException;
    public void setAudiosMarkers(List<Audio> audios, GoogleMap mGoogleMap) throws IOException;
    public void setWrittenNotesMarkers(List<WrittenNote> writtenNotes, GoogleMap mGoogleMap) throws IOException;
    public void setVideosMarkers(List<Video> videos, GoogleMap mGoogleMap) throws IOException;
}
