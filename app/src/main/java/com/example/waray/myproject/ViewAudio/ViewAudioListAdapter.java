package com.example.waray.myproject.ViewAudio;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.MutableDouble;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.waray.myproject.R;
import com.example.waray.myproject.metier.Audio;

import java.net.URI;
import java.util.List;

/**
 * Created by Achrafpc on 03/12/2017.
 */

public class ViewAudioListAdapter extends RecyclerView.Adapter<ViewAudioListAdapter.ViewHolder>{
    private List<Audio> values;
    private Activity activity;
    Dialog MyDialog;
    private MediaPlayer mediaPlayer;
    boolean flage =true;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewDate;
        public View layout;
        private boolean flage = true;



        public ViewHolder(View view) {
            super(view);
            this.textViewDate = (TextView) view.findViewById(R.id.textViewDate);

            this.layout = view;
        }


    }

    public void add(int position, Audio audio){
        values.add(position, audio);
        notifyItemInserted(position);
    }

    public void remove(int position){
        values.remove(position);
        notifyItemRemoved(position);
    }

    public ViewAudioListAdapter(List<Audio> myDataset, Activity activity) {
        values = myDataset;
        this.activity=activity;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.view_audio_item, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;    }

    @Override
    public void onBindViewHolder(final ViewAudioListAdapter.ViewHolder holder, final int position) {
        final Audio audio = new Audio(values.get(position));
        final String date = audio.getDate();
        final String url = audio.getUrl();

        holder.textViewDate.setText(date);
            holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mediaPlayer = MediaPlayer.create(activity.getApplicationContext(),Uri.parse(url));

                    final ImageView ivPlay ;
                    final ImageView ivStop;

                    MyDialog = new Dialog(activity);
                    MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    MyDialog.setContentView(R.layout.alert_audio_layout);
                    MyDialog.setTitle("Play");

                    ivPlay =  MyDialog.findViewById(R.id.play);
                    ivStop =  MyDialog.findViewById(R.id.stop);

                    //detect de end of the audio
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                           ivPlay.setImageResource(R.drawable.icons_play);
                    }
                    });


                    ivPlay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (flage){
                                mediaPlayer = MediaPlayer.create(activity.getApplicationContext() , Uri.parse(url));
                                flage = false ;
                            }
                            if (mediaPlayer.isPlaying() ){
                                mediaPlayer.pause();
                                ivPlay.setImageResource(R.drawable.icons_play);
                            }else{
                                mediaPlayer.start();
                                ivPlay.setImageResource(R.drawable.icons_pause);
                            }
                        }
                    });

                    ivStop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(!flage){
                                mediaPlayer.stop();
                                mediaPlayer.release();
                                flage = true;
                            }
                            ivPlay.setImageResource(R.drawable.icons_play);
                        }
                    });
                    MyDialog.show();

                    MyDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (!flage) {
                                mediaPlayer.stop();
                            }
                        }
                    });


            }
        });


    }


    @Override
    public int getItemCount() {
        return values.size();
    }


}
