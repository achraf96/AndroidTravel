package com.example.waray.myproject.ViewTravel;

import com.example.waray.myproject.metier.Audio;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;

/**
 * Created by smlali on 21/11/17.
 */

interface ViewTravelPresenterItf {

    public Image createImage(String nom, long idVoyage, double lat, double lng, String url);
    public Video createVideo(String nom, long idVoyage, double lat, double lng, String url);

    public Audio createAudio(String date, long idVoyage, double lat, double lng, String url) ;

}
