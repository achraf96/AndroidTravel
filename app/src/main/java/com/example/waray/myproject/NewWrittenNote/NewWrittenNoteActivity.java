package com.example.waray.myproject.NewWrittenNote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.waray.myproject.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NewWrittenNoteActivity extends AppCompatActivity implements NewWrittenNoteActivityItf{


    public NewWrittenNotePresenterItf newWrittenNotePresenterItf;
    public Long ID;
    public Double lat;
    public Double lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_written_note);

        newWrittenNotePresenterItf = new NewWrittenNotePresenter(this);

        final EditText editTextTitle = (EditText) findViewById(R.id.et_title);
        final EditText editTextDescription = (EditText) findViewById(R.id.et_description);

        Intent intent = getIntent();
        ID = intent.getLongExtra("IDVOYAGE",0);
        lat = intent.getDoubleExtra("Lat",0);
        lng = intent.getDoubleExtra("Lng",0);

        Button btnCtreateWrittenNote =(Button) findViewById(R.id.btn_new_note);

        btnCtreateWrittenNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String timeStamp = new SimpleDateFormat("dd/MM/yyyy_HH:mm:ss").format(new Date());

                if(TextUtils.isEmpty(editTextTitle.getText().toString()) || TextUtils.isEmpty(editTextDescription.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Please enter both title and description!",Toast.LENGTH_LONG).show();
                }
                else {
                    //TODO add lat and log
                    newWrittenNotePresenterItf.createWrittenNote(timeStamp ,lat,lng,editTextTitle.getText().toString(),editTextDescription.getText().toString(),ID);
                    Toast.makeText(getApplicationContext(), "WrittenNote added!",Toast.LENGTH_LONG).show();
                    //Intent intent = new Intent(getApplicationContext(),);
                    //intent.putExtra("TITLE",editTextTitle.getText());
                    //startActivity(intent);
                }
            }
        });
    }
}
