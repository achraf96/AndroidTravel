package com.example.waray.myproject.Travels;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.waray.myproject.Db.DAO;
import com.example.waray.myproject.Db.TravelManagerDao;
import com.example.waray.myproject.NewTravel.NewTravelActivity;
import com.example.waray.myproject.R;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Voyage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TravelsActivity extends AppCompatActivity implements TravelsActivityItf{

    private TravelManagerDao travelManagerDao;
    private DAO dao;
    private TravelsPresenterItf travelsPresenter;
    private List<Voyage> voyages = null;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    //private GoogleMap mGoogleMap;
    //private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.travels_activity);
        //if (googleServicesAvailable()) initMap();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //if(googleServicesAvailable()){
        //   Toast.makeText(this, "Ok !", Toast.LENGTH_LONG).show();
        //}

        //CheckUserPermsions();

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        travelsPresenter = new TravelsPresenter(this);

        //travelManagerDao = new TravelManagerDao(getApplicationContext());
        voyages = travelsPresenter.getAllVoyages();

        adapter = new TravelsListAdapter(voyages, this.getApplicationContext());

        recyclerView.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), NewTravelActivity.class);
                startActivity(i);
            }
        });

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder
                            target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        final int position = viewHolder.getAdapterPosition(); //get position which is swipe

                        AlertDialog.Builder builder = new AlertDialog.Builder(recyclerView.getContext()); //alert for confirm to delete
                        builder.setMessage("If you delete this travel, all pictures you took for this travel will be deleted. Are you sure you want to delete?");    //set message

                        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() { //when click on DELETE
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position);    //travels_activity_item removed from recylcerview

                                List<Image> images = travelsPresenter.getImagesByVoyage(voyages.get(position).getId());
                                for (Image image : images) {
                                    //file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), image.getNom()+".jpg");
                                    new File(image.getUrl()).delete();
                                    Toast toast1 = Toast.makeText(getApplicationContext(), image.getUrl(), Toast.LENGTH_LONG);
                                    toast1.show();
                                }
                                travelsPresenter.deleteVoyage(voyages.get(position));
                                //travelManagerDao.deleteVoyage(voyages.get(position));
                                //sqldatabase.execSQL("delete from " + TABLE_NAME + " where _id='" + (position + 1) + "'"); //query for delete
                                voyages.remove(position);  //then remove travels_activity_item

                                return;
                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.notifyItemRemoved(position + 1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                                adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                                return;
                            }
                        }).show();  //show alert dialog

                        //voyages.remove(viewHolder.getAdapterPosition());
                        //adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);


    }

//    private void initMap() {
//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
//        mapFragment.getMapAsync(this);
//    }

    @Override
    public void onResume() {
        voyages = travelsPresenter.getAllVoyages();
        adapter = new TravelsListAdapter(voyages, getApplicationContext());
        recyclerView.setAdapter(adapter);
        super.onResume();
    }

    @Override
    public void onPause() {
        //dao.close();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



//    public Boolean googleServicesAvailable() {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int isAvailabe = apiAvailability.isGooglePlayServicesAvailable(this);
//        if (isAvailabe == ConnectionResult.SUCCESS) {
//            return true;
//        } else if (apiAvailability.isUserResolvableError(isAvailabe)) {
//            Dialog dialog = apiAvailability.getErrorDialog(this, isAvailabe, 0);
//            dialog.show();
//        } else {
//            Toast.makeText(this, "Cant connect to Google services", Toast.LENGTH_LONG).show();
//        }
//        return false;
//    }

//    void getLocation() {
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            if(location!=null){
//                //Toast.makeText(this, "Long : "+String.valueOf(location.getLongitude())+" Lat : "+String.valueOf(location.getLatitude()), Toast.LENGTH_LONG).show();
//            }else{
//                Toast.makeText(this, "No location found", Toast.LENGTH_LONG).show();
//            }
//    }
    //access to permsions
//    void CheckUserPermsions(){
//        if ( Build.VERSION.SDK_INT >= 23){
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
//                    PackageManager.PERMISSION_GRANTED  ){
//                requestPermissions(new String[]{
//                                android.Manifest.permission.ACCESS_FINE_LOCATION},
//                        REQUEST_CODE_ASK_PERMISSIONS);
//                return ;
//            }
//        }
//
//        getLocation();// init the contact list
//
//    }
    //get acces to location permsion
    //final private int REQUEST_CODE_ASK_PERMISSIONS = 123;



    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();// init the contact list
                } else {
                    // Permission Denied
                    Toast.makeText( this,"Noo !!" , Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }*/

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mGoogleMap = googleMap;
//        //goToLocationZoom(39.008224,-76.8984527, 15);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        mGoogleMap.setMyLocationEnabled(true);
// }
//
//    private void goToLocation(double lat, double lng) {
//        LatLng ll = new LatLng(lat, lng);
//        CameraUpdate update = CameraUpdateFactory.newLatLng(ll);
//        mGoogleMap.moveCamera(update);
//    }
//
//    private void goToLocationZoom(double lat, double lng, float zoom) {
//        LatLng ll = new LatLng(lat, lng);
//        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
//        mGoogleMap.moveCamera(update);
//    }
//
//
//    public void geoLocate(View view) throws IOException {
//        EditText et = (EditText) findViewById(R.id.editText);
//        String location = et.getText().toString();
//
//        Geocoder geocoder = new Geocoder(this);
//        List<Address> list = geocoder.getFromLocationName(location, 1);
//        Address address = list.get(0);
//        String locality = address.getLocality();
//
//        Toast.makeText(this, locality, Toast.LENGTH_LONG).show();
//
//        double lat = address.getLatitude();
//        double lng = address.getLongitude();
//
//        goToLocationZoom(lat, lng, 14);
//
//        MarkerOptions options = new MarkerOptions()
//                                    .title(locality)
//                                    .snippet("Picture")
//                                    .position(new LatLng(lat,lng));
//        mGoogleMap.addMarker(options);
//
//    }

}
