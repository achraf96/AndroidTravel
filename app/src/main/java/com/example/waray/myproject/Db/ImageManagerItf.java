package com.example.waray.myproject.Db;

import com.example.waray.myproject.metier.Image;

import java.util.List;

/**
 * Created by achraf on 18/11/2017.
 */

public interface ImageManagerItf {

    public Image createImage(String nom, long idVoyage, double lat, double lng, String url) ;
    public void deleteImage(Image image);
    public List<Image> getImagesByVoyage(Long idVoyage);

    }

