package com.example.waray.myproject.ViewImages;

import com.example.waray.myproject.Db.AppManager;
    import com.example.waray.myproject.Db.ImageManagerItf;
import com.example.waray.myproject.metier.Image;

import java.util.List;

/**
 * Created by achraf on 18/11/2017.
 */

public class ViewImagesPresenter implements ViewImagesPresenterItf {

    protected ViewImagesActivityItf view;

    protected ImageManagerItf imageManager ;

    public ViewImagesPresenter(ViewImagesActivity view) {

        this.view = view;
        imageManager = AppManager.getInstance().getImageManager();

    }

    @Override
    public Image createImage(String nom, long idVoyage, double lat, double lng, String url) {
        return imageManager.createImage(nom,idVoyage, lat, lng, url);
    }

    @Override
    public void deleteImage(Image image) {
         imageManager.deleteImage(image);
    }

    @Override
    public List<Image> getImagesByVoyage(Long idVoyage) {
        return imageManager.getImagesByVoyage(idVoyage);
    }
}
