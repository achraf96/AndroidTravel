package com.example.waray.myproject.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.waray.myproject.metier.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by achraf on 18/11/2017.
 */

public class ImageManagerDao implements ImageManagerItf {

    protected Context context;

    protected SQLiteDatabase database;

    private String[] allColumnsImage = { MySQLiteHelper.COLUMN_Image_ID, MySQLiteHelper.COLUMN_Image_Name,
            MySQLiteHelper.COLUMN_Image_Url,MySQLiteHelper.COLUMN_Image_Lat,
            MySQLiteHelper.COLUMN_Image_Lng, MySQLiteHelper.COLUMN_Image_IdVoyage };


    public ImageManagerDao(Context context) {
        this.context = context;
        database = DAO.getInstance(context);
    }

    public Image createImage(String nom, long idVoyage, double lat, double lng, String url) {

        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_Image_Name, nom);
        values.put(MySQLiteHelper.COLUMN_Image_Url, url);
        values.put(MySQLiteHelper.COLUMN_Image_Lat, lat);
        values.put(MySQLiteHelper.COLUMN_Image_Lng, lng);
        values.put(MySQLiteHelper.COLUMN_Image_IdVoyage, idVoyage);

        long insertId = database.insert(MySQLiteHelper.TABLE_Image, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_Image,
                allColumnsImage, MySQLiteHelper.COLUMN_Image_ID+ " = " + insertId, null,
                null, null, MySQLiteHelper.COLUMN_Image_ID+" DESC");
        cursor.moveToFirst();
        Image newImage = cursorToImage(cursor);
        cursor.close();
        return newImage;
    }

    public void deleteImage(Image image) {
        long id = image.getId();
        database.delete(MySQLiteHelper.TABLE_Image, MySQLiteHelper.COLUMN_Image_ID
                + " = " + id, null);
    }

    public List<Image> getImagesByVoyage(Long idVoyage) {
        List<Image> images = new ArrayList<Image>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_Image,
                allColumnsImage, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Image image= cursorToImage(cursor);
            if (image.getIdVoyage() == idVoyage){
                images.add(image);
            }
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return images;
    }


    private Image cursorToImage(Cursor cursor) {
        Image image = new Image();
        image.setId(cursor.getLong(0));
        image.setNom(cursor.getString(1));
        image.setUrl(cursor.getString(2));
        image.setLat(cursor.getDouble(3));
        image.setLng(cursor.getDouble(4));
        image.setIdVoyage(cursor.getLong(5));
        return image;
    }
}
