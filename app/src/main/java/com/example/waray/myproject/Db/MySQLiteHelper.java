package com.example.waray.myproject.Db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Waray on 11/10/2017.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_VOYAGE = "voyage";
    public static final String COLUMN_VOYAGE_ID = "id";
    public static final String COLUMN_VOYAGE_TITRE = "titre";
    public static final String COLUMN_VOYAGE_DESCRIPTION = "description";

    public static final String TABLE_Image = "Image";
    public static final String COLUMN_Image_ID = "idImage";
    public static final String COLUMN_Image_Name = "nom";
    public static final String COLUMN_Image_Url = "url";
    public static final String COLUMN_Image_Lat = "lat";
    public static final String COLUMN_Image_Lng = "lng";
    public static final String COLUMN_Image_IdVoyage = "idVoyage";

    public static final String TABLE_Video = "Video";
    public static final String COLUMN_Video_ID = "idVideo";
    public static final String COLUMN_Video_Name = "nom";
    public static final String COLUMN_Video_Url = "url";
    public static final String COLUMN_Video_Lat = "lat";
    public static final String COLUMN_Video_Lng = "lng";
    public static final String COLUMN_Video_IdVoyage = "idVoyage";


    public static final String TABLE_Audio = "Audio";
    public static final String COLUMN_Audio_ID = "idAudio";
    public static final String COLUMN_Audio_DATE = "date";
    public static final String COLUMN_Audio_Url = "url";
    public static final String COLUMN_Audio_Lat = "lat";
    public static final String COLUMN_Audio_Lng = "lng";
    public static final String COLUMN_Audio_IdVoyage = "idVoyage";


    public static final String TABLE_WrittenNote = "WrittenNote";
    public static final String COLUMN_WrittenNote_ID = "idWrittenNote";
    public static final String COLUMN_WrittenNote_Title = "title";
    public static final String COLUMN_WrittenNote_Description = "description";
    public static final String COLUMN_WrittenNote_DATE = "date";
    public static final String COLUMN_WrittenNote_Lat = "lat";
    public static final String COLUMN_WrittenNote_Lng = "lng";
    public static final String COLUMN_WrittenNote_IdVoyage = "idVoyage";

    // You may use your own database name
    private static final String DATABASE_NAME = "myDatabase.db";
    private static final int DATABASE_VERSION = 4;

    // Database creation sql statement
    private static final String DATABASE_CREATE_VOYAGE = "CREATE TABLE "
            + TABLE_VOYAGE + "("  + COLUMN_VOYAGE_ID + " INTEGER PRIMARY KEY autoincrement , "
            + COLUMN_VOYAGE_TITRE + " TEXT NOT NULL , "
            + COLUMN_VOYAGE_DESCRIPTION + " TEXT NOT NULL ); ";


    private static final String DATABASE_CREATE_IMAGE = "CREATE TABLE "+ TABLE_Image + "(" + COLUMN_Image_ID
            + " INTEGER PRIMARY KEY autoincrement, "
            + COLUMN_Image_Name + " TEXT NOT NULL, "
            + COLUMN_Image_Url + " TEXT NOT NULL, "
            + COLUMN_Image_Lat + " REAL, "
            + COLUMN_Image_Lng + " REAL, "
            + COLUMN_Image_IdVoyage + " INTEGER );";

    private static final String DATABASE_CREATE_VIDEO = "CREATE TABLE "+ TABLE_Video + "(" + COLUMN_Video_ID
            + " INTEGER PRIMARY KEY autoincrement, "
            + COLUMN_Video_Name + " TEXT NOT NULL, "
            + COLUMN_Video_Url + " TEXT NOT NULL, "
            + COLUMN_Video_Lat + " REAL, "
            + COLUMN_Video_Lng + " REAL, "
            + COLUMN_Video_IdVoyage + " INTEGER );";

    private static final String DATABASE_CREATE_AUDIO = "CREATE TABLE "+ TABLE_Audio + "(" + COLUMN_Audio_ID
            + " INTEGER PRIMARY KEY autoincrement, "
            + COLUMN_Audio_DATE + " TEXT NOT NULL, "
            + COLUMN_Audio_Url + " TEXT NOT NULL, "
            + COLUMN_Audio_Lat + " REAL, "
            + COLUMN_Audio_Lng + " REAL, "
            + COLUMN_Audio_IdVoyage + " INTEGER );";

    private static final String DATABASE_CREATE_WrittenNote = "CREATE TABLE "+ TABLE_WrittenNote + "(" + COLUMN_WrittenNote_ID
            + " INTEGER PRIMARY KEY autoincrement, "
            + COLUMN_WrittenNote_DATE + " TEXT NOT NULL, "
            + COLUMN_WrittenNote_Lat + " REAL, "
            + COLUMN_WrittenNote_Lng + " REAL, "
            + COLUMN_WrittenNote_Title + " TEXT NOT NULL , "
            + COLUMN_WrittenNote_Description + " TEXT NOT NULL ,  "
            + COLUMN_WrittenNote_IdVoyage + " INTEGER );";



    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE_VOYAGE);
        database.execSQL(DATABASE_CREATE_IMAGE);
        database.execSQL(DATABASE_CREATE_VIDEO);
        database.execSQL(DATABASE_CREATE_AUDIO);
        database.execSQL(DATABASE_CREATE_WrittenNote);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VOYAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Image);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Video);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Audio);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WrittenNote);
        onCreate(db);
    }

}
