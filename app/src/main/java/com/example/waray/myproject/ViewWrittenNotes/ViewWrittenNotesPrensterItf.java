package com.example.waray.myproject.ViewWrittenNotes;

import com.example.waray.myproject.metier.WrittenNote;

import java.util.List;

/**
 * Created by Achrafpc on 17/12/2017.
 */

interface ViewWrittenNotesPrensterItf {

    public List<WrittenNote> getWrittenNotesByVoyage(Long idVoyage) ;

    public void deleteWritenNote(WrittenNote writtenNote);
}
