package com.example.waray.myproject.Travels;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.ImageManagerItf;
import com.example.waray.myproject.Db.TravelManagerItf;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Voyage;

import java.util.List;

/**
 * Created by Waray on 18/11/2017.
 */

public class TravelsPresenter implements TravelsPresenterItf {

    protected TravelsActivityItf view;

    protected TravelManagerItf travelManager;

    protected ImageManagerItf imageManager;

    public TravelsPresenter(TravelsActivity view){

        this.view = view;

        travelManager = AppManager.getInstance().getTravelManager();
        imageManager = AppManager.getInstance().getImageManager();
    }

    @Override
    public List<Voyage> getAllVoyages() {

        return travelManager.getAllVoyages();
    }

    @Override
    public void deleteVoyage(Voyage voyage) {

        travelManager.deleteVoyage(voyage);
    }

    @Override
    public List<Image> getImagesByVoyage(Long idVoyage) {

        return imageManager.getImagesByVoyage(idVoyage);
    }


}
