package com.example.waray.myproject.NewTravel;

/**
 * Created by Waray on 18/11/2017.
 */

interface NewTravelActivityItf {

    public void onResume();

    public void onPause();

}
