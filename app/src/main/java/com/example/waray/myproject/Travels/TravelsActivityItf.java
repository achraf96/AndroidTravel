package com.example.waray.myproject.Travels;

import android.view.Menu;
import android.view.MenuItem;

import com.example.waray.myproject.metier.Voyage;

import java.util.List;

/**
 * Created by Waray on 18/11/2017.
 */

interface TravelsActivityItf {

    public void onResume();

    public void onPause();

    public boolean onCreateOptionsMenu(Menu menu);

    public boolean onOptionsItemSelected(MenuItem item);
}
