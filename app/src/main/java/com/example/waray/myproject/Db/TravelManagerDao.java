package com.example.waray.myproject.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.waray.myproject.metier.Voyage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waray on 18/11/2017.
 */

public class TravelManagerDao implements TravelManagerItf {

    protected Context context;

    protected SQLiteDatabase database;

    private String[] allColumnsVoyage = { MySQLiteHelper.COLUMN_VOYAGE_ID,
            MySQLiteHelper.COLUMN_VOYAGE_TITRE, MySQLiteHelper.COLUMN_VOYAGE_DESCRIPTION };

    public TravelManagerDao(Context context){
        this.context = context;
        database = DAO.getInstance(context);
    }

    @Override
    public Voyage createVoyage(String titre, String description) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_VOYAGE_TITRE, titre);
        values.put(MySQLiteHelper.COLUMN_VOYAGE_DESCRIPTION, description);
        long insertId = database.insert(MySQLiteHelper.TABLE_VOYAGE, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_VOYAGE,
                allColumnsVoyage, MySQLiteHelper.COLUMN_VOYAGE_ID+ " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Voyage newVoyage = cursorToVoyage(cursor);
        cursor.close();
        return newVoyage;
    }

    @Override
    public void deleteVoyage(Voyage voyage) {
        long id = voyage.getId();
        System.out.println("Voyage deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_Image, MySQLiteHelper.COLUMN_Image_IdVoyage
                + " = " + id, null);
        database.delete(MySQLiteHelper.TABLE_VOYAGE, MySQLiteHelper.COLUMN_VOYAGE_ID
                + " = " + id, null);
    }

    @Override
    public List<Voyage> getAllVoyages() {
        List<Voyage> voyages = new ArrayList<Voyage>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_VOYAGE,
                allColumnsVoyage, null, null, null, null, MySQLiteHelper.COLUMN_VOYAGE_ID+" DESC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Voyage voyage= cursorToVoyage(cursor);
            voyages.add(voyage);
            cursor.moveToNext();
        }
        // assurez-vous de la fermeture du curseur
        cursor.close();
        return voyages;
    }

    @Override
    public Voyage cursorToVoyage(Cursor cursor) {
        Voyage voyage = new Voyage();
        voyage.setId(cursor.getLong(0));
        voyage.setTitre(cursor.getString(1));
        voyage.setDescription(cursor.getString(2));
        return voyage;
    }
}
