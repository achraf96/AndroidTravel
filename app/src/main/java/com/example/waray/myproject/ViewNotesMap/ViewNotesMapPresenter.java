package com.example.waray.myproject.ViewNotesMap;

import com.example.waray.myproject.Db.AppManager;
import com.example.waray.myproject.Db.AudioManagerItf;
import com.example.waray.myproject.Db.ImageManagerItf;
import com.example.waray.myproject.Db.VideoManagerItf;
import com.example.waray.myproject.Db.WrittenNoteManagerItf;
import com.example.waray.myproject.metier.Audio;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;
import com.example.waray.myproject.metier.WrittenNote;

import java.util.List;

/**
 * Created by Achrafpc on 21/12/2017.
 */

public class ViewNotesMapPresenter implements ViewNotesMapPresenterItf{

    protected ViewNotesMapActivityItf view;

    protected ImageManagerItf imageManager ;
    protected AudioManagerItf audioManagerItf;
    protected VideoManagerItf videoManagerItf;
    protected WrittenNoteManagerItf writtenNoteManagerItf;

    public ViewNotesMapPresenter(ViewNotesMapActivityItf view) {

        this.view = view;
        imageManager = AppManager.getInstance().getImageManager();
        audioManagerItf = AppManager.getInstance().getAudioManager();
        videoManagerItf = AppManager.getInstance().getVideoManager();
        writtenNoteManagerItf = AppManager.getInstance().getWrittenNoteManager();
    }
    @Override
    public List<Image> getImagesByVoyage(Long idVoyage) {
        return imageManager.getImagesByVoyage(idVoyage);
    }

    @Override
    public List<Audio> getAudiosByVoyage(Long idVoyage) {
        return audioManagerItf.getAudiosByVoyage(idVoyage);
    }

    @Override
    public List<Video> getVideosByVoyage(Long idVoyage) {
        return videoManagerItf.getVideoByVoyage(idVoyage);
    }

    @Override
    public List<WrittenNote> getWrittenNotesByVoyage(Long idVoyage) {
        return writtenNoteManagerItf.getWrittenNotesByVoyage(idVoyage);
    }
}
