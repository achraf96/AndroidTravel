package com.example.waray.myproject.ViewNotesMap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.waray.myproject.Map.Map;
import com.example.waray.myproject.R;
import com.example.waray.myproject.metier.Audio;
import com.example.waray.myproject.metier.Image;
import com.example.waray.myproject.metier.Video;
import com.example.waray.myproject.metier.WrittenNote;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class ViewNotesMapActivity extends AppCompatActivity implements ViewNotesMapActivityItf, OnMapReadyCallback {
    GoogleMap mGoogleMap;
    List<Audio> audios;
    List<Image> images;
    List<WrittenNote> writtenNotes;
    List<Video> videos;
    ViewNotesMapPresenterItf viewNotesMapPresenterItf;
    Long ID;
    private Map map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notes_map);

        map = new Map(getApplicationContext(), this);
        if (map.googleServicesAvailable()) initMap();

        Intent intent =getIntent();
        ID = intent.getLongExtra("IDVOYAGE",0);
        viewNotesMapPresenterItf = new ViewNotesMapPresenter(this);
        audios = viewNotesMapPresenterItf.getAudiosByVoyage(ID);
        images = viewNotesMapPresenterItf.getImagesByVoyage(ID);
        writtenNotes = viewNotesMapPresenterItf.getWrittenNotesByVoyage(ID);
        videos = viewNotesMapPresenterItf.getVideosByVoyage(ID);
    }

    public void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment96);
        mapFragment.getMapAsync(this);
    }

    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        //goToLocationZoom(39.008224,-76.8984527, 15);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        try {

            setImagesMarkers(images, mGoogleMap);
            setAudiosMarkers(audios, mGoogleMap);
            setVideosMarkers(videos, mGoogleMap);
            setWrittenNotesMarkers(writtenNotes, mGoogleMap);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setImagesMarkers(List<Image> images, GoogleMap mGoogleMap) throws IOException {
        Geocoder geocoder = new Geocoder(this);

        Iterator<Image> iterator = images.iterator();
        while (iterator.hasNext()) {
            Image image = iterator.next();
            if (image.getLat()!=0 && image.getLng()!=0){
                MarkerOptions options = new MarkerOptions()
                    .title(geocoder.getFromLocation(image.getLat(), image.getLng(), 1).get(0).getLocality())
                    .snippet("Picture")
                    .position(new LatLng(image.getLat(),image.getLng()));
                mGoogleMap.addMarker(options);
            }
        }

    }

    @Override
    public void setAudiosMarkers(List<Audio> audios, GoogleMap mGoogleMap) throws IOException {
        Geocoder geocoder = new Geocoder(this);

        Iterator<Audio> iterator = audios.iterator();
        while (iterator.hasNext()) {
            Audio audio = iterator.next();
            if (audio.getLat()!=0 && audio.getLng()!=0){
            MarkerOptions options = new MarkerOptions()
                    .title(geocoder.getFromLocation(audio.getLat(), audio.getLng(), 1).get(0).getLocality())
                    .snippet("Audio")
                    .position(new LatLng(audio.getLat(),audio.getLng()));
            mGoogleMap.addMarker(options);}
        }
    }

    @Override
    public void setWrittenNotesMarkers(List<WrittenNote> writtenNotes, GoogleMap mGoogleMap) throws IOException {
        Geocoder geocoder = new Geocoder(this);

        Iterator<WrittenNote> iterator = writtenNotes.iterator();
        while (iterator.hasNext()) {
            WrittenNote writtenNote = iterator.next();
            if (writtenNote.getLat()!=0 && writtenNote.getLng()!=0) {
                MarkerOptions options = new MarkerOptions()
                        .title(geocoder.getFromLocation(writtenNote.getLat(), writtenNote.getLng(), 1).get(0).getLocality())
                        .snippet("WrttenNote")
                        .position(new LatLng(writtenNote.getLat(), writtenNote.getLng()));
                mGoogleMap.addMarker(options);
            }
        }
    }

    @Override
    public void setVideosMarkers(List<Video> videos, GoogleMap mGoogleMap) throws IOException {
        Geocoder geocoder = new Geocoder(this);

        Iterator<Video> iterator = videos.iterator();
        while (iterator.hasNext()) {
            Video video = iterator.next();
            if (video.getLat()!=0 && video.getLng()!=0) {
                MarkerOptions options = new MarkerOptions()
                        .title(geocoder.getFromLocation(video.getLat(), video.getLng(), 1).get(0).getLocality())
                        .snippet("Video")
                        .position(new LatLng(video.getLat(), video.getLng()));
                mGoogleMap.addMarker(options);
            }
        }
    }
}
