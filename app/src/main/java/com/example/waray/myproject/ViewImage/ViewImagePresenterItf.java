package com.example.waray.myproject.ViewImage;

import com.example.waray.myproject.metier.Image;

/**
 * Created by Waray on 20/12/2017.
 */

public interface ViewImagePresenterItf {
    public void deleteImage(Image image);
}
